# crunch binary

Binary build of `crunch`, a wordlist generator.

https://gitlab.com/pgregoire-ci/crunch/-/jobs/artifacts/main/raw/crunch.tar.gz?job=build

## References

https://sourceforge.net/projects/crunch-wordlist/
